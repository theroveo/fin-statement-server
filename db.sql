SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `fins` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `fins` ;

-- -----------------------------------------------------
-- Table `fins`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fins`.`user` (
  `email` VARCHAR(100) NOT NULL,
  `surname` VARCHAR(50) NULL,
  `firstname` VARCHAR(30) NULL,
  PRIMARY KEY (`email`),
  UNIQUE INDEX `EMAIL_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fins`.`import`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fins`.`import` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) NOT NULL,
  `date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `email`),
  UNIQUE INDEX `ID_UNIQUE` (`id` ASC),
  INDEX `fk_import_user_idx` (`email` ASC),
  CONSTRAINT `fk_import_user`
    FOREIGN KEY (`email`)
    REFERENCES `fins`.`user` (`email`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fins`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fins`.`category` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(100) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `ID_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fins`.`transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fins`.`transaction` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `import_id` INT UNSIGNED NOT NULL,
  `category_id` INT UNSIGNED NOT NULL,
  `amount` VARCHAR(30) NULL,
  `account_balance` VARCHAR(30) NULL,
  PRIMARY KEY (`id`, `import_id`, `category_id`),
  INDEX `fk_transaction_import1_idx` (`import_id` ASC),
  INDEX `fk_transaction_category1_idx` (`category_id` ASC),
  UNIQUE INDEX `TRANSACTION_ID_UNIQUE` (`id` ASC),
  CONSTRAINT `fk_transaction_import1`
    FOREIGN KEY (`import_id`)
    REFERENCES `fins`.`import` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `fins`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fins`.`category_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fins`.`category_type` (
  `ID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `ID_UNIQUE` (`ID` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fins`.`category_spend`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fins`.`category_spend` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` INT UNSIGNED NOT NULL,
  `transaction_id` VARCHAR(45) NOT NULL,
  `amount` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`, `category_id`),
  UNIQUE INDEX `ID_UNIQUE` (`id` ASC),
  INDEX `fk_category_spend_category1_idx` (`category_id` ASC),
  INDEX `fk_category_spend_transaction1_idx` (`transaction_id` ASC),
  CONSTRAINT `fk_category_spend_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `fins`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_category_spend_transaction1`
    FOREIGN KEY (`transaction_id`)
    REFERENCES `fins`.`transaction` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
