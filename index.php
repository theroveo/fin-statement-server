<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

// autoload my own classes
spl_autoload_register(function ($classname) {
    require ("src/classes/" . $classname . ".php");
});

// app options
$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;
$config['db']['host']   = "172.17.0.3";
$config['db']['user']   = "root";
$config['db']['pword']   = "yes";
$config['db']['dbname'] = "fins";


// initialise PHPSlim
$app = new \Slim\App(["settings" => $config]);


// get container object
$container = $app->getContainer();

//using dependency injection, specify db connection settings
$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'],
        $db['user'], $db['pword']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};







// HTTP GET request mapped to URL: /fins.api/tx
$app->get('/fins.api/get/imports', function (Request $request, Response $response) {

    $mapper = new Imports($this->db);
    $tickets = $mapper->getImports();

    $response->getBody()->write(var_export($tickets, true));
    return $response;
});

// HTTP GET request mapped to URL: /fins.api/{name}
$app->get('/fins.api/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});
$app->run();