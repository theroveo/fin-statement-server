Getting and installing Slim
	run composer require slim/slim
	the dependencies will be placed inside the vendor directory

Database
	Make sure the database is running
	Edit the database configuration section of index.php to match your environment

Check functionality
	point your browsr to one of the endpoints in the app (/fins.api/get/imports)