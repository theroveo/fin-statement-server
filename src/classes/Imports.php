<?php

class Imports {

	private $_db;

	function __construct($db){
		$this->_db = $db;
	}


	public function getImports() {

		$sth = $this->_db->prepare("SELECT email, date FROM import");
		$sth->execute();
		$result = $sth->fetchAll();
	
		return $result;		
	}

}